package com.ata.core

class UserRepository(private val sesi: SessionManager) {

    companion object {
        @Volatile
        private var instance: UserRepository? = null

        fun getInstance(sesi: SessionManager): UserRepository =
            instance ?: synchronized(this) {
                instance
                    ?: UserRepository(sesi)
            }
    }

    fun loginUser(username: String) {
        sesi.createLoginSession()
        sesi.saveToPreference(SessionManager.KEY_USERNAME, username)
    }

    fun getUser() = sesi.getFromPreference(SessionManager.KEY_USERNAME)

    fun isUserLogin() = sesi.isLogin

    fun logoutUser() = sesi.logout()

    fun setFilm(): String {
        val film = FilmEntity("test", "test", "test", "test", "test", "test", 1)
        return  film.title.toString()
    }

    fun getFilmGenre(): String {
        val film = FilmEntity("test", "test", "test", "test", "test", "test", 1)
        return film.genre.toString()
    }
}